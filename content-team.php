<div class="row">

					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 team">
							<?php $title=get_the_title(); ?>
							<h2 class="team_title">Meet a few of our Los Angeles <?php echo $title; ?> Teachers</h2>
							<p class="team-des">  because we've done all the hard work for you! Each of our teachers has completed a rigorous screening process, including an application  </p>
							<div class="row">
									<?php 	

							$rd_args = array('posts_per_page' =>3,'post_type'  => 'team');
									 	
							$the_query = new WP_Query( $rd_args);
									
							if ( $the_query->have_posts() ) {
								while ( $the_query->have_posts() ) { $the_query->the_post();

									$name=get_post_meta(get_the_ID(),'name', true);
									$position=get_post_meta(get_the_ID(),'position',true);
									$excellence=get_post_meta(get_the_ID(),'excellence',true);
									//echo $excellence;
									$t=explode(',',$excellence);
									$t=array_map('strtolower', $t);
									// echo "<pre>";
									// print_r($t);
									// echo "</pre>";

									//if (in_array(strtolower($title),$t)){
									if (preg_grep( "/".$title."/i" , $t )){
									
							?>
								
								<div class="col-xs-6 col-sm-6 col-md-3 col-lg-3 memeber">
								 <a href="<?php the_permalink();?>">	
								  <?php the_post_thumbnail('team-image',array('class'=>'member_img') );?> </a>
									<h2 class="member_name"><?php echo $name;?></h2>
									<h3 class="member_post"><?php // echo $position;?> </h3>
								</div>

							<?php } } } else {

									echo "No post found here";
					
							} wp_reset_postdata(); ?>

								<!-- <div class="col-xs-6 col-sm-6 col-md-2 col-lg-2 pull-right team_contact">
									<h2 class="contact-us">Contact us</h2>
								<p class="contact-des">For more information about the School of Music,<br><strong>call us now at <br>818-209-2620</strong></p>
								</div> -->
							</div>
					</div>
				</div>