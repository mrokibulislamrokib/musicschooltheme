<?php get_header();?>
		

		<?php get_template_part('content','slider');?>
		
		<section class="content">
			<div class="container">

				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-9 col-lg-9">
						
						<?php if(have_posts()): ?>
					<input type="hidden" class="this_course" value="<?php the_ID();?>">
							<h2 class="lessson"> <span style="font-weight:bold;"><?php 			the_title();?> Lessons</span></h2>
							<!--<h3 class="guitar"><?php the_title();?> </h3>-->
							<hr/>
							<?php the_content(); ?> 
							

						<?php endif;?>

					</div>
					
					<?php get_sidebar();?>


				</div>

				<?php get_template_part('content','team');?>

			</div>

		</section>

		<?php  get_template_part('content','contact');?>


<?php get_footer();?>
<script type="text/javascript">
var url="<?php echo get_template_directory_uri(); ?>";
	jQuery(function($){
		
		var name=$(".this_course").val();
		$(".owl-item a [course-image='"+name+"']").parent().css("background-image",'url("'+url+'/images/bg_img_slider_color.png")');
		$(".owl-item a [course-image='"+name+"']").parent().css("margin-top","20px");
	    $(".wp-newsfw-list li:nth-child(2) a:first-child").remove();
		$(".wp-newsfw-list li").each(function(){
			
			//$(this).find('time').remove();
		});
	});
</script>