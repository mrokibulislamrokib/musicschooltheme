jQuery(document).ready(function($) {
		
		if($('.bl_categories').length){
		 	var pb = $(".bl_categories").owlCarousel({
		 		itemsCustom :[[1199,5],[992,4],[768,3],[480,2],[300,2]],
		 		autoPlay : true,
		 		stopOnHover : true,
		 		slideSpeed : 600,
		 		addClassActive : true
		 	});

		 	$('.pb_prev').on('click',function(){
		 		pb.trigger('owl.prev');
		 	});

		 	$('.pb_next').on('click',function(){
		 		pb.trigger('owl.next');
		 	});
		}

		$('.flexslider').flexslider({
			    animation: "slide"
			  });

		// $('#menu').slicknav();
		$('#menu').slicknav({
			label: 'Click Here for Menu',
			duration: 500,
			
		});
});