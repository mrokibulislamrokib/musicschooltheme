<?php /*
Template Name: Blog page
*/ get_header();?>

		<?php  get_template_part('content','slider');?>

		
		
		<section class="content">
			<div class="container">

					<div class="blog_heading">
						<h1>Los Angeles Music Teachers </h1>
						<h2>Welcome to our Blog</h2>
					</div>
					



		<?php
		    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		    $args = array( 
		        'posts_per_page' => 5, 
		        'paged' => $paged, 
		        'post_type' => 'post'
		    );
		    $cpt_query = new WP_Query($args);
		?>

		<?php if ($cpt_query->have_posts()) : while ($cpt_query->have_posts()) : $cpt_query->the_post(); ?>

		   	<div class="row blog_post">
					
					    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

							<div class="row">
								<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
									<div class="post-date">
										<h4 class="date"><?php the_time('j') ?> </h4>
										<h6 class="month"><?php the_time('F ');?></h6>
									</div>
									
								</div>
								<div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<h2 class="post-title"> <?php the_title();?></h2>
<!-- 											<h4 class="post-meta">Lorem Ipsum Dolor</h4> -->
											<p class="post-description">
												<?php   $content=preg_replace(array('<p>','</p>'),array('',''),get_the_content($params),1);
 echo wp_trim_words($content, 80, '...' );
												// $content = get_the_content();  
												// echo wpautop($content); ?> 
											</p>

											<a href="<?php the_permalink();?>" class="readmore">Read More</a>
											
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<?php the_post_thumbnail('blog-image');?>
										</div>

									</div>
									

									

								</div>


							</div>

						</div>
					
					</div>

		<?php endwhile; endif; ?>

		<!-- <nav>
		    <ul>
		        <li><?php previous_posts_link( '&laquo; PREV', $cpt_query->max_num_pages) ?></li> 
		        <li><?php next_posts_link( 'NEXT &raquo;', $cpt_query->max_num_pages) ?></li>
		    </ul>
		</nav> -->
	<?php 	kriesi_pagination($cpt_query->max_num_pages); ?>
			</div>

		</section>
		
		<?php get_template_part('content','contact'); ?>

		
<?php get_footer();?>


