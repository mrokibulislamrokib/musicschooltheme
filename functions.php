<?php
	

	// if (!class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/Redux/ReduxCore/framework.php' ) ) {
	//     	require_once( dirname( __FILE__ ) . '/Redux/ReduxCore/framework.php' );
	// }
	// if (!isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/Redux/sample/sample-config.php' ) ) {
	//     require_once( dirname( __FILE__ ) . '/Redux/sample/sample-config.php' );
	// }
	
	require_once 'includes/wp_bootstrap_navwalker.php';
	
	add_theme_support( 'post-thumbnails', array( 'post','team','course'));
	add_theme_support( 'automatic-feed-links' );
	add_image_size( 'post-image', 450,420,true );
	add_image_size( 'team-image', 180,180,true );
	add_image_size( 'course-image', 460,220,true );
	add_image_size( 'course-slider-image', 180,140,true );
	add_image_size( 'blog-image',334,284,true );
	set_post_thumbnail_size( 150, 150 );

	function my_sidebar(){

			$args = array(
				'name'          => __('course sidebar'),
				'id'            => 'course-sidebar-id',
				'description'   => '',
				'class'         => '',
				'before_widget' => '<li><a href="">',
				'after_widget'  => '</a></li>',
				'before_title'  => '<h2 class="sidebar_header">',
				'after_title'   => '</h2>'
			);
		
			register_sidebar( $args );



			   /**
				* Creates a sidebar
				* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
				*/
				$newsargs = array(
					'name'          => __( 'Newsletter', 'theme_text_domain' ),
					'id'            => 'newsletter-id',
					'description'   => '',
					'class'         => '',
					'before_widget' => '',
					'after_widget'  => '',
					'before_title'  => '<h2>',
					'after_title'   => '</h2>'
				);
			
				register_sidebar($newsargs);
			
	}

	add_action('widgets_init','my_sidebar');


	if(!function_exists('register_menus')) {
		function register_menus() {
			register_nav_menus(array(
				'main-menu' => __('Main menu'),
			));
		}
	}
	add_action('init', 'register_menus');


	function theme_name_scripts() {
	
		wp_enqueue_style("bootstrap_css",get_template_directory_uri().'/css/bootstrap.min.css');
		wp_enqueue_style("owl_carousel",get_template_directory_uri().'/css/owl.carousel.css');
		wp_enqueue_style("owl_transitions",get_template_directory_uri().'/css/owl.transitions.css');
		wp_enqueue_style("flx_css",get_template_directory_uri().'/flexslider.css');
		wp_enqueue_style("slick_css",get_template_directory_uri().'/css/slicknav.css');
		wp_enqueue_style("css",get_template_directory_uri().'/style.css');;
		wp_enqueue_script("jquery");
		//	wp_deregister_script('jQuery');
		//	wp_enqueue_script("jquery", get_template_directory_uri().'/js/jquery-2.1.0.min.js', array(), false, true);
		wp_enqueue_script("bootstrap-js", get_template_directory_uri().'/js/bootstrap.min.js', array(), false, true);
		wp_enqueue_script("flex-js", get_template_directory_uri().'/js/jquery.flexslider-min.js', array(), false, true);
	   	wp_enqueue_script("carousel-js", get_template_directory_uri().'/js/owl.carousel.min.js', array(), false, true);
	   	wp_enqueue_script("slick-js", get_template_directory_uri().'/js/jquery.slicknav.min.js', array(), false, true);
	   	wp_enqueue_script("scripts-js", get_template_directory_uri().'/js/script.js', array(), false, true);
	   	wp_localize_script('function','ajax_script',array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ));
	}

	add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );


	function custom_post() {
	
		$labels = array(
			'name'                => __( 'Course'),
			'singular_name'       => __( 'course'),
			'add_new'             => __( 'Add New course'),
			'add_new_item'        => __( 'Add New course'),
			'edit_item'           => __( 'Edit course'),
			'new_item'            => __( 'New course'),
			'view_item'           => __( 'View course'),
			'search_items'        => __( 'Search Course'),
			'not_found'           => __( 'No Course found'),
			'not_found_in_trash'  => __( 'No Course found in Trash'),
			'parent_item_colon'   => __( 'Parent course:'),
			'menu_name'           => __( 'Course', 'text-domain' ),
		);
	
		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array(),
			'public'              => TRUE,
			'show_ui'             => TRUE,
			'show_in_menu'        => TRUE,
			'show_in_admin_bar'   => TRUE,
			'menu_position'       => null,
			'menu_icon'           => null,
			'show_in_nav_menus'   => TRUE,
			'publicly_queryable'  => TRUE,
			'exclude_from_search' => false,
			'has_archive'         => TRUE,
			'query_var'           => TRUE,
			'can_export'          => TRUE,
			'rewrite'             => array ( 'slug' => 'course' ),
			'capability_type'     => 'post',
			'supports'            => array('title', 'editor','thumbnail')
		);
	
		register_post_type( 'course', $args );
		
				$cat_labels = array(
			'name'					=> __( 'Category'),
			'singular_name'			=> __( 'Category'),
			'search_items'			=> __( 'Search Category'),
			'popular_items'			=> __( 'Popular Category'),
			'all_items'				=> __( 'All Category'),
			'parent_item'			=> __( 'Parent Category'),
			'parent_item_colon'		=> __( 'Parent Category'),
			'edit_item'				=> __( 'Edit Category'),
			'update_item'			=> __( 'Update Category'),
			'add_new_item'			=> __( 'Add New Category'),
			'new_item_name'			=> __( 'New Category'),
			'add_or_remove_items'	=> __( 'Add or remove Category'),
			'choose_from_most_used'	=> __( 'Choose from most used Category'),
			'menu_name'				=> __( 'Category'),
		);
		
		$cat_args = array(
			'labels'            => $cat_labels,
			'public'            => true,
			'show_in_nav_menus' => true,
			'show_admin_column' => false,
			'hierarchical'      => false,
			'show_tagcloud'     => true,
			'show_ui'           => true,
			'query_var'         => true,
			'rewrite'           => true,
			'query_var'         => true,
			'capabilities'      => array(),
		);
	
		register_taxonomy( 'course-category', array( 'course' ),$cat_args);

		
			$team_labels = array(
				'name'                => __( 'Teacher'),
				'singular_name'       => __( 'Teacher'),
				'add_new'             => __( 'Add New Teacher'),
				'add_new_item'        => __( 'Add New Teacher'),
				'edit_item'           => __( 'Edit Teacher'),
				'new_item'            => __( 'New Teacher'),
				'view_item'           => __( 'View Teacher'),
				'search_items'        => __( 'Search Teacher'),
				'not_found'           => __( 'No Teacher found'),
				'not_found_in_trash'  => __( 'No Teacher found in Trash'),
				'parent_item_colon'   => __( 'Parent Teacher:'),
				'menu_name'           => __( 'Teacher'),
			);
		
			$team_args = array(
				'labels'              => $team_labels,
				'hierarchical'        => false,
				'description'         => 'description',
				'taxonomies'          => array(),
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'show_in_admin_bar'   => true,
				'menu_position'       => null,
				'menu_icon'           => null,
				'show_in_nav_menus'   => true,
				'publicly_queryable'  => true,
				'exclude_from_search' => false,
				'has_archive'         => true,
				'query_var'           => true,
				'can_export'          => true,
				'rewrite'             => true,
				'capability_type'     => 'post',
				'supports'            => array('title', 'editor','thumbnail','comments')
			);
		
			register_post_type( 'team',$team_args);
		
	}
	
	add_action( 'init', 'custom_post' );
	



	function custom_meta() {
			add_meta_box( 'team_meta', __( 'Team Meta'), 'custom_meta_callback', 'team' );
	}
	add_action( 'add_meta_boxes', 'custom_meta' );

	function custom_meta_callback($post) {
			wp_nonce_field( basename( __FILE__ ), 'team_nonce' );
			$custom_stored_meta = get_post_meta( $post->ID );
	?>

		<p>
			<label for="name" class="prfx-row-title"><?php _e( 'Member Name')?></label>
			<input type="text" name="name" id="name" value="<?php if (isset($custom_stored_meta['name'] ) ) echo $custom_stored_meta['name'][0]; ?>" />
		</p>

		<p>
			<label for="position" class="prfx-row-title"><?php _e( 'position')?></label>
			<input type="text" name="position" id="position" value="<?php if (isset($custom_stored_meta['position'] ) ) echo $custom_stored_meta['position'][0]; ?>" />
		</p>

		<p>
			<label for="facebook_link" class="prfx-row-title"><?php _e( 'Facebook Link')?></label>
			<input type="text" name="facebook_link" id="facebook_link" value="<?php if (isset($custom_stored_meta['facebook_link'] ) ) echo $custom_stored_meta['facebook_link'][0]; ?>" />
		</p>

		<p>
			<label for="Twitter_link" class="prfx-row-title"><?php _e( ' Twitter Link')?></label>
			<input type="text" name="Twitter_link" id="Twitter_link" value="<?php if (isset($custom_stored_meta['Twitter_link'] ) ) echo $custom_stored_meta['Twitter_link'][0]; ?>" />
		</p>
		<p>
			<label for="google_plus" class="prfx-row-title"><?php _e( 'Google PLus Link')?></label>
			<input type="text" name="google_plus" id="google_plus" value="<?php if (isset($custom_stored_meta['google_plus'] ) ) echo $custom_stored_meta['google_plus'][0]; ?>" />
		</p>
		<p>
			<label for="linkedin_link" class="prfx-row-title"><?php _e( 'Linkedin Link')?></label>
			<input type="text" name="linkedin_link" id="linkedin_link" value="<?php if (isset($custom_stored_meta['linkedin_link'] ) ) echo $custom_stored_meta['linkedin_link'][0]; ?>" />
		</p>

		<p>
			<label for="excellence" class="prfx-row-title"><?php _e( 'excellence')?></label>
			<input type="text" name="excellence" id="excellence" value="<?php if (isset($custom_stored_meta['excellence'] ) ) echo $custom_stored_meta['excellence'][0]; ?>" />
		</p>
		
		<p>
			<label for="teach" class="prfx-row-title"><?php _e( 'teach')?></label>
			<input type="text" name="teach" id="teach" value="<?php if (isset($custom_stored_meta['teach'] ) ) echo $custom_stored_meta['teach'][0]; ?>" />
		</p>

		<?php	}


		function prfx_meta_save( $post_id ) {
			// Checks save status
			$is_autosave = wp_is_post_autosave($post_id);
			$is_revision = wp_is_post_revision( $post_id );
			$is_valid_nonce = ( isset( $_POST[ 'team_nonce' ] ) && wp_verify_nonce( $_POST[ 'team_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
			// Exits script depending on save status
			if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
				return;
			}
			// Checks for input and sanitizes/saves if needed

			if( isset( $_POST[ 'name' ] ) ) {
				update_post_meta( $post_id, 'name', sanitize_text_field( $_POST[ 'name'] ) );
			}

			if( isset( $_POST[ 'position' ] ) ) {
				update_post_meta( $post_id, 'position', sanitize_text_field( $_POST[ 'position' ] ) );
			}

			if( isset( $_POST[ 'facebook_link' ] ) ) {
				update_post_meta( $post_id, 'facebook_link', sanitize_text_field( $_POST[ 'facebook_link' ] ) );
			}

			if( isset( $_POST[ 'Twitter_link' ] ) ) {
				update_post_meta( $post_id, 'Twitter_link', sanitize_text_field( $_POST[ 'Twitter_link' ] ) );
			}

			if( isset( $_POST[ 'google_plus' ] ) ) {
				update_post_meta( $post_id, 'google_plus', sanitize_text_field( $_POST[ 'google_plus' ] ) );
			}

			if( isset( $_POST[ 'linkedin_link' ] ) ) {
				update_post_meta( $post_id, 'linkedin_link', sanitize_text_field( $_POST[ 'linkedin_link' ] ) );
			}
			
			if( isset( $_POST[ 'excellence' ] ) ) {
				update_post_meta( $post_id, 'excellence', sanitize_text_field( $_POST[ 'excellence' ] ) );
			}

			if( isset( $_POST[ 'teach' ] ) ) {
				update_post_meta( $post_id, 'teach', sanitize_text_field( $_POST[ 'teach' ] ) );
			}

		}
		add_action( 'save_post', 'prfx_meta_save' );



		function kriesi_pagination($pages = '', $range = 2){  
    		
    		 $showitems = ($range * 2)+1;  

    		 global $paged;
    
    		 if(empty($paged)) $paged = 1;

     		if($pages == ''){
         
         	global $wp_query;
         	$pages = $wp_query->max_num_pages;
         
         	if(!$pages){
             	$pages = 1;
        	}
     	}   

     	if(1 != $pages){
         
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}