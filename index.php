<?php get_header();?>

		<div class="clearfix slider_div">
			<?php echo do_shortcode('[fit_fs_slider id="1"]');?>
			<!-- <div class="flexslider">
			  <ul class="slides">
			    <li>	
			      	<img src="<?php echo get_template_directory_uri(); ?>/images/slider-image-1.png" />
			   		
			   		

			    </li>
			  
			  </ul>
			</div> -->
			<div class="clearfix"></div>
			<div class="text_top_slider">
				<h2 class="color_yellow">Los angeles</h2>
				<h2 class="color_red">music teachers</h2>
				
			</div>
			<?php echo do_shortcode('[contact-form-7 id="50" title="Contact form 1"]');?>

		</div>

		   		
		<section class="content clearfix">

			<div class="about_us" style="min-height:120px;">
				<div class="container">
					<h2 style = "color: #fffddb;">About us</h2>
					<p class="home_paragraph" style="color: #fffddb; padding-top: 0px;">Los Angeles Music Teachers is committed to bringing back the importance of music education for our young  students since it is sorely lacking in  our schools. We use contemporary music to teach students while enforcing  traditional music teaching disciplines for a strong foundation. </p>
				</div>
			</div>

			<div class="container">
				
				<br>
				<h2 style = "color: #C7060B; text-align: center;">What Instrument Would You Like To Learn?</h2> <br><br>

					<div class="row clearfix">


		<?php 	

			$rd_args = array('posts_per_page' => 12,'post_type'  => 'course');
					 	
			$the_query = new WP_Query( $rd_args);
					
			if ( $the_query->have_posts() ) {
				
				while ( $the_query->have_posts() ) { $the_query->the_post();?>
						 <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
							<div class="about_data">
					<a href="<?php the_permalink();?>"> 	<?php the_post_thumbnail('');?> </a> 
								<!-- <img src="images/about-image.jpg"> -->
								<h2> <a href="<?php the_permalink();?>"> 
								<?php the_title();?> </a> </h2>
							</div>	
						</div>

			<?php	} } else { echo "No post found here"; } wp_reset_postdata(); ?>
					
			</div>
					
				</div>
		</section>


		<div class="newsletter clearfix">
			<div class="container">	

				<?php if ( is_active_sidebar( 'newsletter-id')):?>
						<?php dynamic_sidebar( 'newsletter-id'); ?>
				<?php endif; ?>
				
				<!-- <h2>Newsletter</h2>
				<form class="form-horizontal contact-form" role="form">
					<div class="form-group">
						<input type="text" name="" id="" class="" placeholder="Your email to subscribe">
						<input type="submit" value="Submit">
					</div>
				</form> -->
			</div>
		</div>


<?php get_footer();?>