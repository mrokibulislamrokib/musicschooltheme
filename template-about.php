<?php 
/*
Template Name: About page
*/
get_header();?>

	<section class="content clearfix">
		<div class="container" style="min-height: 1000px;">
	
			<?php if(have_posts()): while(have_posts()) : the_post(); ?>

				<?php the_content();?>

			<?php endwhile; else: ?>

				<h3><?php _e('No posts were found!') ?></h3>

			<?php endif; ?>
			
		</div>
	</section>

<?php get_footer();?>