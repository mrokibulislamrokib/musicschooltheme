<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
	<?php if ( is_active_sidebar( 'course-sidebar-id')):?>
			<?php dynamic_sidebar( 'course-sidebar-id'); ?>
	<?php endif; ?>

	<div class="contact_me">
            <h2 class="contact-us">Contact us</h2>
									<p class="contact-des">For more information about the School of Music,<br><strong>call us now at <br>818-209-2620</strong></p>
	</div>
</div>