<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<title>Los Angeles Music Teachers</title>
	<?php wp_head();?>
</head>
<body>

<header class="header">
			<div class="container">
					<div class="row">
						<div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
							<a href="<?php bloginfo('url'); ?>" class="logo">
								<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="">
							</a>

						</div>
						
						<div class="col-xs-6 col-sm-8 col-md-8 col-lg-8">
							<div class="call_us_mobile"><a href="tel:8182092620">CALL US NOW <br>(818) 209-2620</a></div>
							<nav class="navbar header_nav" role="navigation">
								<!-- Brand and toggle get grouped for better mobile display -->
								<!-- <div class="navbar-header">
									<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
							
								<!-- Collect the nav links, forms, and other content for toggling -->
							<!-- 	<div class="collapse navbar-collapse navbar-ex1-collapse"> --> 
									<?php  
										$defaults = array(
											'theme_location'  => 'main-menu',
											'menu'            => '',
											'container'       => false,
											'container_class' => '',
											'container_id'    => '',
											'menu_class'      => '',
											'menu_id'         => '',
											'echo'            => true,
											'fallback_cb'     => 'wp_page_menu',
											'before'          => '',
											'after'           => '',
											'link_before'     => '',
											'link_after'      => '',
											'items_wrap'      => '<ul id="menu" class="nav navbar-nav navbar-right %2$s">%3$s</ul>',
											'depth'           => 0,
											'walker'          => ''
										);
										wp_nav_menu( $defaults );
									?> 

									<!-- <ul class="nav navbar-nav navbar-right">
										<li class="active"><a href="#">Home</a></li>
									</ul> -->
								<!-- </div> -->
								
							</nav>
						</div>

					</div>
			</div>
		</header>