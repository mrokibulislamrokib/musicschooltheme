<?php get_header();?>

		<?php get_template_part('content','slider2');?>
		
		<section class="content">
			<div class="container">

				<?php if(have_posts()): 
				$position=get_post_meta(get_the_ID(),'position',true);
				$facebook=get_post_meta(get_the_ID(),'facebook_link',true);
				$twitter=get_post_meta(get_the_ID(),'Twitter_link',true);
				$google=get_post_meta(get_the_ID(),'google_plus',true); 
				$linkedin=get_post_meta(get_the_ID(),'linkedin_link',true);
				$excellence=get_post_meta(get_the_ID(),'excellence',true);
?>

					
					<div class="teacher_content">
						<span class="cross"><a href="<?php echo site_url(); ?>/teacher">x</a></span>
						<div class="row">
							<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 teacher_img">
								<?php the_post_thumbnail('team-image'); ?>
							</div>
								<div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 teacher_dd">
									<input type="hidden" class="this_techer" value="<?php the_ID();?>">
									<h2 class="teacher-name"><?php the_title();?></h2>
									<h4 class="teacher-meta"><?php echo $excellence;?></h4>
									<p class="teacher-des"><?php the_content(); ?></p>

									<div class="row">
										<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
											<!-- <h2 class="teaching-system">Leave your Comment</h2> -->

											<?php// comments_template(); ?>

										<!-- 	<form class="comment_form" method="post" action="">
												
												<div class="form-group">
													<input type="text" name="" id="" class="comment-name" placeholder="Your Name">
												</div>

												<div class="form-group">
													<textarea placeholder="Your Comment" class="comment-message"></textarea>
												</div>

												<div class="form-group">
													<input type="submit" value="Send" class="comment-submit">
												</div>

											</form> -->
										</div>

										<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 social_div">
											<div class="follow">

												<h3 class="follow">follow on</h3>

												<ul class="social-image">
													<?php if(isset($facebook)){?>
														<li><a href="<?php echo $facebook;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/facebook.png" alt=""></a></li>
													<?php } ?>
													<?php if(isset($twitter)){?>
														<li><a href="<?php echo $twitter;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/twitter.png" alt=""></a></li>
													<?php } ?>
													<?php if(isset($google)){?>
														<li><a href="<?php echo $google;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/google-plus.png" alt=""></a></li>
													<?php } ?>
													<?php if(isset($linkedin)) { ?>
														<li><a href="<?php echo $linkedin;?>"><img src="<?php echo get_template_directory_uri(); ?>/images/linkedin.png" alt=""></a></li>
													<?php } ?>
												</ul>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
				<?php endif;?>
			</div>

		</section>
		
<?php get_footer();?>
<script type="text/javascript">
var url="<?php echo get_template_directory_uri(); ?>";
	jQuery(function($){
		$(".nav li a:contains('Teacher')").parent().addClass('current-menu-parent');
		var name=$(".this_techer").val();
		$(".owl-item a [techer-image='"+name+"']").parent().css("background-image",'url("'+url+'/images/bg_img_slider_color.png")');
		$(".owl-item a [techer-image='"+name+"']").parent().css("margin-top","20px");
	});
</script>