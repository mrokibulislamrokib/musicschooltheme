<?php get_header();?>

	<section class="content clearfix">
		<div class="container">
	
			<?php if(have_posts()): while(have_posts()) : the_post(); ?>

				<?php the_content();?>

			<?php endwhile; else: ?>

				<h3><?php _e('No posts were found!') ?></h3>

			<?php endif; ?>
			
		</div>
	</section>
<style type="text/css">
	footer{
		  position: absolute;
		  width: 100%;
		  bottom: 0px;
		
	}
	.wpcf7{
		margin-left: -20px;
		 width: 96% !important;
	}

	


	@media screen and (max-width: 1400px) {
			.contact_we{
				min-height: 450px !important;
				margin-bottom:160px; 
			}

			footer{
					  position: inherit;
					  width: 100%;
					  bottom: 0px;
					
				}
			.copyright-right{
				padding: 0px;
			}
			.footer_bottom{
				min-height: 85px;
				height: auto;
			}
	}
</style>

<?php get_footer();?>