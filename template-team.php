<?php /*
Template Name: Team page
*/ get_header();?>

	<section class="content clearfix">
	   <div class="container team_page">
			<div class="row clearfix">



			   		<?php 	

					$rd_args = array('posts_per_page' => 12,'post_type'  => 'team');
							 	
					$the_query = new WP_Query( $rd_args);
							
					if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) { $the_query->the_post();?>
						
						<div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
							<div class="about_data">
							<a href="<?php the_permalink();?>">	
								<?php the_post_thumbnail('team-image');?></a>
								<!-- <img src="images/about-image.jpg"> -->
								<h2><?php the_title();?></h2>
								<span class="t_span">Click to view bio</span>
							</div>	
						</div>

					<?php } } else {

							echo "No post found here";
			
					} wp_reset_postdata(); ?>
			</div>
		</div>
	</section>


<?php get_footer();?>

<style type="text/css">
	footer{
		  position: absolute;
		  width: 100%;
		  bottom: 0px;
		
	}

	@media screen and (max-width: 640px) {
			.team_page{
				min-height: 450px !important;
			}

			footer{
					  position: inherit;
					  width: 100%;
					  bottom: 0px;
					
				}
			.copyright-right{
				padding: 0px;
			}
			.footer_bottom{
				min-height: 85px;
				height: auto;
			}
	}
</style>