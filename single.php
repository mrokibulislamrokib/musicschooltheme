<?php get_header();?>

		<?php get_template_part('content','slider');?>

		<section class="content">
			<div class="container">


			<?php if(have_posts()): ?>
				<div class="row blog_post">
					
					    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

							<div class="row">
								<div class="col-xs-2 col-sm-2 col-md-1 col-lg-1">
									<div class="post-date">
										<h4 class="date"><?php the_time('j') ?> </h4>
										<h6 class="month"><?php the_time('F ');?></h6>
									</div>
									
								</div>

								<div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<h2 class="post-title"> <?php the_title();?></h2>
											<p class="post-description">
												<?php the_content(); ?>
											</p>
											
										</div>
										
										<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
											<?php the_post_thumbnail('post-image');?>
										</div>

									</div>
								</div>


							</div>

						</div>
					
					</div>

				<?php comments_template();?>

			<?php endif;?>
		</div>
	</section>

<?php get_footer();?>